call plug#begin('~/.local/share/nvim/plugged')

" File Explorer - Nerd Tree
Plug 'scrooloose/nerdtree'
Plug 'xuyuanp/nerdtree-git-plugin'

" Info Tabline - Airline
Plug 'bling/vim-airline'
Plug 'tpope/vim-fugitive'

" Text Navigation - Easymotion
Plug 'easymotion/vim-easymotion'

" Code Completion - Deoplete
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'tweekmonster/deoplete-clang2'
Plug 'deoplete-plugins/deoplete-clang'

" Auto completion: braces
Plug 'jiangmiao/auto-pairs'

" Themes
Plug 'rakr/vim-two-firewatch'
Plug 'arcticicestudio/nord-vim'
Plug 'mhartington/oceanic-next'
Plug 'AlessandroYorba/Sierra'

" Multicursor
Plug 'terryma/vim-multiple-cursors'

" Fuzzy finding
Plug 'itspriddle/vim-fuzzyfinder-pathogen'

" Easymotion
Plug 'easymotion/vim-easymotion'

" GDB Debugging support
Plug 'Mistobaan/pyclewn'

" Show marks
Plug 'vim-scripts/ShowMarks'

" Cmake project integration
Plug 'vhdirk/vim-cmake'


call plug#end()

" Tabs
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>i
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Open terminal in split below
command! -nargs=* T split | terminal <args>
" Exit terminal using <Esc>
tnoremap <Esc> <C-\><C-n>

" True color
set termguicolors

" Add shortcut ctrl+k, ctrl+b to toggle nerd tree
"nnoremap <silent> <C-k><C-B> :NERDTreeToggle<CR>
autocmd BufWinEnter * NERDTreeMirror

" Open nerd tree on startup
autocmd VimEnter * NERDTree | wincmd p
" Close nerdtree if it is the last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Deoplete and clang
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#auto_complete_start_length = 1
"let g:deoplete#sources#clang#executable = '/usr/lib/llvm/8/bin/clang'
"let g:deoplete#sources#clang#clang_complete_database = './build'

" Use tab for completion
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Theme
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled = 1

set background=dark
let g:two_firewatch_italics=1
colorscheme two-firewatch
let g:airline_theme = 'twofirewatch'

"colorscheme nord
"let g:airline_theme = 'nord'

syntax on
"let g:oceanic_next_terminal_bold = 1
"let g:oceanic_next_terminal_italic = 1
"colorscheme OceanicNext
"let g:airline_theme='oceanicnext'

"let g:sierra_Sunset = 1
"colorscheme sierra

" Editor
set tabstop=2
set softtabstop=2
set shiftwidth=2

set shiftround

set expandtab
set encoding=utf-8
set fileencodings=utf-8
set autoread
set number
"set hidden

set ruler

